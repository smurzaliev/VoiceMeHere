//
//  ViewController.swift
//  VoiceMeHere
//
//  Created by Samat Murzaliev on 04.04.2023.
//

import AgoraChat
import AgoraRtcKit
import AVFoundation
import AVKit
import InstantSearchVoiceOverlay
import SnapKit
import UIKit

class ViewController: UIViewController {
       
    var agoraKit: AgoraRtcEngineKit!
    
    // By default, set the current user role to broadcaster to both send and receive streams.
    var userRole: AgoraClientRole = .broadcaster
    let userRoles = ["Broadcaster", "Audience"]
    let chatUserIds = ["User1", "User2"]
    var currentChatUser = 0
    var remoteUser = 1
    let userIDs = ["user1_fghuy", "user2_fghuy"]
    let userTokens = ["007eJxTYOA3iDTcxCRiaRn6V+pG22Zjv0WG93+wvgo6FGEm80f+1QMFBjMD45SklDQTQ0sjY5M0k1QLQ5PENGPDxFSjRKCMZWrzOr2UhkBGBt6Fn5gZGVgZGIEQxFdhMDJMSk5KNTPQTTE2N9c1NExN0U0ytTTStTBPMbI0tExLSjVJAwC9EiZD", "007eJxTYPio8fj/cw4lS5OHX242ZpcapGbP1HBaJahvof9/F4PHvAwFBjMD45SklDQTQ0sjY5M0k1QLQ5PENGPDxFSjRKCMZeqUdXopDYGMDH5aW5kZGVgZGIEQxFdhMDY1Mjc3MjPQTTE2N9c1NExN0U0yMzTSTbE0MDEwTDI3TzK2AAAVgiVb"]
    var lastMessageText = ""
    var channelName = "demoChannel"
    
    let haptic = UISelectionFeedbackGenerator()
    
    var joined: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.buttonStack.isHidden = !self.joined
                self.joinButton.setTitle( self.joined ? "Leave" : "Join", for: .normal)
            }
        }
    }
    
    let voiceOverlay = VoiceOverlayController()
    let synthesizer = AVSpeechSynthesizer()
    
    let buttonStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 30
        stack.alignment = .center
        return stack
    }()
    
    lazy var joinButton: UIButton = {
        let button = UIButton()
        let config = UIButton.Configuration.bordered()
        button.configuration = config
        button.setTitle("Join", for: .normal)
        button.addTarget(self, action: #selector(joinButtonAction), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        let config = UIButton.Configuration.bordered()
        button.configuration = config
        button.backgroundColor = .green
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Login", for: .normal)
        button.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        button.isEnabled = true
        return button
    }()
    
    lazy var logoutButton: UIButton = {
        let button = UIButton()
        let config = UIButton.Configuration.bordered()
        button.configuration = config
        button.backgroundColor = .red
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Logout", for: .normal)
        button.addTarget(self, action: #selector(logoutAction), for: .touchUpInside)
        button.isEnabled = true
        return button
    }()
    
    let speechText: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.numberOfLines = 0
        return view
    }()
    
    lazy var userRoleToggle: UISegmentedControl = {
        let view = UISegmentedControl(items: userRoles)
        view.selectedSegmentIndex = 0
        view.addTarget(self, action: #selector(userRoleChanged), for: .valueChanged)
        view.isEnabled = false
        return view
    }()
    
    lazy var userChatIDToggle: UISegmentedControl = {
        let view = UISegmentedControl(items: chatUserIds)
        view.selectedSegmentIndex = 0
        view.addTarget(self, action: #selector(userIDChanged), for: .valueChanged)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        haptic.prepare()
        setupView()
        initializeAgoraEngine()
        initChatSDK()
    }
    
    private func prepareSpeakers() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .spokenAudio, options: [.mixWithOthers, .defaultToSpeaker])
//            try! AVAudioSession.sharedInstance().setCategory(.playAndRecord)
//            try AVAudioSession.sharedInstance().setMode(.spokenAudio)
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        } catch let error {
            print("Failed to set audio session category, mode, or override output to speaker: \(error.localizedDescription)")
        }
    }
    
    private func setupView() {
        view.backgroundColor = .darkGray
        view.addSubview(joinButton)
        joinButton.snp.makeConstraints { make in
            make.height.equalTo(60)
            make.width.equalTo(100)
            make.center.equalToSuperview()
        }
        
        view.addSubview(speechText)
        speechText.snp.makeConstraints { make in
            make.top.equalTo(joinButton.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.height.equalTo(150)
        }
        
        view.addSubview(userRoleToggle)
        userRoleToggle.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.bottom.equalTo(joinButton.snp.top).offset(-50)
            make.centerX.equalToSuperview()
        }
        
        view.addSubview(logoutButton)
        logoutButton.snp.makeConstraints { make in
            make.height.equalTo(60)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(userRoleToggle.snp.top).offset(-30)
        }
        
        view.addSubview(loginButton)
        loginButton.snp.makeConstraints { make in
            make.height.equalTo(60)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(logoutButton.snp.top).offset(-10)
        }
        
        view.addSubview(userChatIDToggle)
        userChatIDToggle.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.bottom.equalTo(loginButton.snp.top).offset(-10)
            make.centerX.equalToSuperview()
        }
        
        buildStack()
        view.addSubview(buttonStack)
        buttonStack.isHidden = !joined
        buttonStack.snp.makeConstraints { make in
            make.height.equalTo(64)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-100)
        }
    }
    
    private func buildStack() {
        
        let muteButton: UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "btn_mute"), for: .normal)
            button.addTarget(self, action: #selector(didClickMuteButton), for: .touchUpInside)
            return button
        }()
        
        let endCallButton: UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "btn_endcall"), for: .normal)
            button.addTarget(self, action: #selector(didClickHangUpButton), for: .touchUpInside)
            return button
        }()
        
        let speakerButton: UIButton = {
            let button = UIButton()
            button.setImage(UIImage(named: "btn_speaker_blue"), for: .normal)
            button.addTarget(self, action: #selector(didClickSwitchSpeakerButton), for: .touchUpInside)
            return button
        }()
        
        buttonStack.addArrangedSubview(muteButton)
        buttonStack.addArrangedSubview(endCallButton)
        buttonStack.addArrangedSubview(speakerButton)
    }
    
    func initChatSDK() {
        // Replaces <#Agora App Key#> with your own App Key.
        // Initializes the Agora Chat SDK.
        let options = AgoraChatOptions(appkey: "71941085#1099639")
        options.isAutoLogin = false // Disables auto login.
        options.enableConsoleLog = true
        AgoraChatClient.shared().initializeSDK(with: options)
        // Adds the chat delegate to receive messages.
        AgoraChatClient.shared().chatManager?.add(self, delegateQueue: nil)
    }
    
    func checkForPermissions() async -> Bool {
        let hasPermissions = await self.avAuthorization(mediaType: .audio)
        return hasPermissions
    }

    func avAuthorization(mediaType: AVMediaType) async -> Bool {
        let mediaAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
        switch mediaAuthorizationStatus {
        case .denied, .restricted: return false
        case .authorized: return true
        case .notDetermined:
            return await withCheckedContinuation { continuation in
                AVCaptureDevice.requestAccess(for: mediaType) { granted in
                    continuation.resume(returning: granted)
                }
            }
        @unknown default: return false
        }
    }
    
    func initializeAgoraEngine() {
        let config = AgoraRtcEngineConfig()
        // Pass in your App ID here.
        config.appId = AppID
        // Use AgoraRtcEngineDelegate for the following delegate parameter.
        agoraKit = AgoraRtcEngineKit.sharedEngine(with: config, delegate: self)
    }
    
    func joinChannel() async {
        if await !self.checkForPermissions() {
            showMessage(title: "Error", text: "Permissions were not granted")
            return
        }

        let option = AgoraRtcChannelMediaOptions()

        // Set the client role option as broadcaster or audience.
        if self.userRole == .broadcaster {
            option.clientRoleType = .broadcaster
        } else {
            option.clientRoleType = .audience
        }

        // For an audio call scenario, set the channel profile as communication.
        option.channelProfile = .communication

        // Join the channel with a temp token and channel name
        let result = agoraKit.joinChannel(
            byToken: Token, channelId: channelName, uid: 0, mediaOptions: option,
            joinSuccess: { (channel, uid, elapsed) in }
        )

        // Check if joining the channel was successful and set joined Bool accordingly
        if (result == 0) {
            joined = true
            showMessage(title: "Success", text: "Successfully joined the channel as \(self.userRole)")
            if self.userRole == .broadcaster {
                speechText.text = ""
                voiceOverlay.delegate = self
                voiceOverlay.settings.autoStart = true
                voiceOverlay.settings.autoStop = true
                voiceOverlay.settings.showResultScreen = false
                voiceOverlay.settings.autoStopTimeout = 7
                voiceOverlay.settings.layout.inputScreen.backgroundColor = .clear
                
                voiceOverlay.start(on: self, textHandler: { text, final, _ in
                    if final {
                        print("Final text: \(text)")
                    } else {
                        self.speechText.text = text
                    }
                }) { error in
                    print("voice overlay error: \(error?.localizedDescription)")
                }
            } else if userRole == .audience {
                agoraKit.adjustAudioMixingVolume(12)

            }
        }
    }

    func leaveChannel() {
        let result = agoraKit.leaveChannel(nil)
        // Check if leaving the channel was successful and set joined Bool accordingly
        if result == 0 { joined = false }
    }

    
    @objc func didClickHangUpButton(_ sender: UIButton) {
        leaveChannel()
    }
    
    @objc func joinButtonAction(sender: UIButton!) {
        if !joined {
            sender.isEnabled = false
            Task {
                await joinChannel()
                sender.isEnabled = true
            }
        } else {
            leaveChannel()
        }
    }
    
    
    func showMessage(title: String, text: String, delay: Int = 2) -> Void {
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
            self.present(alert, animated: true)
            alert.dismiss(animated: true, completion: nil)
        })
    }
    
    @objc func didClickMuteButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        // Stops/Resumes sending the local audio stream.
        agoraKit.muteLocalAudioStream(sender.isSelected)
    }
    
    @objc func didClickSwitchSpeakerButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        // Enables/Disables the audio playback route to the speakerphone.
        //
        // This method sets whether the audio is routed to the speakerphone or earpiece. After calling this method, the SDK returns the onAudioRouteChanged callback to indicate the changes.
        agoraKit.setEnableSpeakerphone(sender.isSelected)
    }
    
    @objc func userRoleChanged() {
        haptic.selectionChanged()
        if userRoleToggle.selectedSegmentIndex == 0 {
            self.userRole = .broadcaster
        } else if userRoleToggle.selectedSegmentIndex == 1 {
            self.userRole = .audience
        }
    }
    
    @objc func userIDChanged() {
        haptic.selectionChanged()
        if userChatIDToggle.selectedSegmentIndex == 0 {
            self.currentChatUser = 0
            self.remoteUser = 1
        } else if userChatIDToggle.selectedSegmentIndex == 1 {
            self.currentChatUser = 1
            self.remoteUser = 0
        }
    }
}

extension ViewController: AgoraRtcEngineDelegate, VoiceOverlayDelegate {
    func recording(text: String?, final: Bool?, error: Error?) {
        if let error1 = error {
            print(error1.localizedDescription)
            return
        }
        
        guard let status = final, let message = text else {return}
        
        if !status {
            print(message)
            let finalArray = lastMessageText.components(separatedBy: " ")
            let newArray = message.components(separatedBy: " ")
            let diff = newArray.filter{!finalArray.contains($0)}
            let diffString = diff.joined(separator: " ")
            lastMessageText = message
            sendAction(message: diffString)
        }
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        
    }
}

extension ViewController: AgoraChatManagerDelegate {

    // Logs in with the token.
    @objc func loginAction() {
        let userId = self.userIDs[currentChatUser]
        let token = self.userTokens[currentChatUser]
        let err = AgoraChatClient.shared.login(withUsername: userId, agoraToken: token)
        if err == nil {
            print("login success")
            userRoleToggle.isEnabled = true
            joinButton.isEnabled = true
        } else {
            print("login failed:\(err?.errorDescription ?? "")")
        }
    }

    // Logs out.
    @objc func logoutAction() {
        AgoraChatClient.shared().logout(false) { err in
            if err == nil {
                print("logout success")
                self.userRoleToggle.isEnabled = false
                self.joinButton.isEnabled = false
            }
        }
    }

    // Sends a text message.
    func sendAction(message: String) {
        let remoteUser = userIDs[remoteUser]
        let text = message
        guard let currentUserName = AgoraChatClient.shared().currentUsername else {
            print("Not login or remoteUser/text is empty")
            return
        }
        let msg = AgoraChatMessage(
            conversationId: remoteUser, from: currentUserName,
            to: remoteUser, body: .text(content: text), ext: nil
        )
        
        AgoraChatClient.shared().chatManager?.send(msg, progress: nil) { msg, err in
            if let err = err {
                print("send msg error.\(err.errorDescription)")
            } else {
                print("send msg success")
            }
        }
    }
    func messagesDidReceive(_ aMessages: [AgoraChatMessage]) {
        for msg in aMessages {
            switch msg.swiftBody {
            case let .text(content):
                self.speechText.text = content
                self.synthesizer.stopSpeaking(at: .word)
                try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .spokenAudio, options: [.interruptSpokenAudioAndMixWithOthers])
                let utterance = AVSpeechUtterance(string: content)
                utterance.rate = AVSpeechUtteranceDefaultSpeechRate
                utterance.volume = 85
                utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
                self.synthesizer.speak(utterance)
            default:
                break
            }
        }
    }
}


